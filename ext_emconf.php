<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'HIVE>Form POST Finisher',
    'description' => 'Provides HTTP POST finisher for EXT:form',
    'category' => 'misc',
    'author' => 'teufels GmbH',
    'author_email' => 'digital@teufels.com',
    'state' => 'stable',
    'version' => '1.0.0',
    'constraints' => [
        'depends' =>
            [
                'typo3' => '11.5.0-11.9.99',
                'form' => '11.5.0-11.9.99',
            ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
