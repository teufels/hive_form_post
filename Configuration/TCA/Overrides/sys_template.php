<?php
defined('TYPO3_MODE') || die();

$extKey = 'hive_form_post';
$title = 'HIVE>Form POST Finisher';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extKey, 'Configuration/TypoScript', $title);