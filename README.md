![VENDOR](https://img.shields.io/badge/vendor-HIVE-%219A83.svg)
![KEY](https://img.shields.io/badge/key-hive--form--post-blue.svg)
![version](https://img.shields.io/badge/version-1.0.*-yellow.svg?style=flat-square)

# Custom form finisher "HTTP POST/GET"

This TYPO3 extension adds a custom form finisher "HTTP POST/GET" to the
TYPO3 form framework which call plain HTTP Request (POST/GET) to transfer data via cURL.
The transmitted Data will be generated as array from the Form Fields.

## Install
Copy the extension folder to `\typo3conf\ext\ `, upload it via extension
manager or add it to your composer.json. 
Add the static TypoScript configuration to your TypoScript template.

#### This version supports TYPO3
![CUSTOMER](https://img.shields.io/badge/11_LTS-%23A6C694.svg?style=flat-square)

#### Composer support
`composer req beewilly/hive_form_post`

## Usage
1. Add Finisher "HTTP POST/GET" to your form
2. Set target URL in the finisher
3. Optional: Set username/password in the finisher if authentication is required
4. Optional: Set additional variables that are needed (e.g: optinSetupId for MailingWork)
5. Optional: Activate "Convert field key to integer" if target needs keys to be integer
   * id must be included in identifier
   * (hidden) honeypot key could not be converted -> may need to be disabled in the form.yaml
6. The transmitted Form Data will be generated automatically as array from the Fields identifier as key and value as value
- for Testing https://webhook.site could be used

## Customization
- tbd.

## Documentation
- tbd.

## Changelog
### [1.0.0] - 2022-11-29
- release version
### [0.4.0] - 2022-11-23
- add Finisher option to change the HTTP Request Method between POST and GET
### [0.3.0] - 2022-11-23
- add Finisher option to convert post field key as integer instead of string -> id must be included in identifier
### [0.2.0] - 2022-11-16
- add manually creatable variables to the finisher backend
- make url required
### [0.1.0] - 2022-11-14
- initial
